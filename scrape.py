from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from BookScraper.spiders.bookScraper import BookScraper

# Запуск процесса извлечения данных
process = CrawlerProcess(get_project_settings())
process.crawl(BookScraper)
process.start()

input()
