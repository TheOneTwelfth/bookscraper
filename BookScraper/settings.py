BOT_NAME = 'BookScraper'

SPIDER_MODULES = ['BookScraper.spiders']
NEWSPIDER_MODULE = 'BookScraper.spiders'

ROBOTSTXT_OBEY = True

LOG_ENABLED = False
