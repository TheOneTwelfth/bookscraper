import scrapy

SCRAPE_URL = "http://books.toscrape.com/"  # URL для извлечения данных


class BookScraper(scrapy.Spider):

    name = "BookScraper"  # Название кроулера

    def start_requests(self):  # Входная функция
        yield scrapy.Request(url=SCRAPE_URL, callback=self.parse)

    def parse(self, response):  # Функция для парсинга страниц
        # Извлечение и направление в стандартный поток вывода названий всех книг на странице
        for book in response.css("article.product_pod"):
            title = book.css("h3 a::attr(title)").extract_first()
            print(title)

        # Переход на следующую страницу, если она существует
        next_page = response.css("li.next a::attr(href)").extract_first()
        if next_page is not None:
            next_url = response.urljoin(next_page)
            yield scrapy.Request(url=next_url, callback=self.parse)
